/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import Historia.HerramientasHistorial;
import Historia.Historial;
import lo.de.lazaro.Personal.Factory;
import lo.de.lazaro.Personal.Personal;

/**
 *
 * @author sergiod
 */
public class main {

    public static void main(String[] arg) {

        Personal pers1 = Factory.crearPersonal("medico");
        System.out.println(pers1.getTipoPersonal());

        Personal pers2 = Factory.crearPersonal("ayudante");
        System.out.println(pers2.getTipoPersonal());

        Historial[] historiales = new Historial[2];

        historiales[0] = HerramientasHistorial.crearHistorial(pers1, "nombrePaciente1");
        historiales[1] = HerramientasHistorial.crearHistorial(pers2, "nombrePaciente2");

        if (historiales[0] != null) {
            System.out.println(historiales[0].getNombre());
        } else {
            System.out.println("null");
        }
        if (historiales[1] != null) {
            System.out.println(historiales[1].getNombre());
        } else {
            System.out.println("null");
        }
    }
}
