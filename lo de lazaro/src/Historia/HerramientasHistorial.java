/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Historia;

import lo.de.lazaro.Personal.Personal;

/**
 *
 * @author sergiod
 */
public abstract class HerramientasHistorial {

    public static Historial crearHistorial(Personal per, String n) {
        String p = per.getTipoPersonal();
        switch (p) {
            case ("Medico"):
                return new Historial(n);
            case ("Ayudante"):
                // no tiene permisos
                return null;
            default:
                // no tiene permisos
                return null;
        }
    }
}
